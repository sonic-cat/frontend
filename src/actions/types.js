export const AUTH_USER = 'auth_user';
export const UNAUTH_USER = 'unauth_user';
export const AUTH_ERROR = 'auth_error';
export const REGISTER_USER = 'register_user';
export const REGISTER_ERROR = 'register_error';

export const SET_PLAYLIST = 'set_playlist';
export const PLAY_ENTRY = 'play_entry';
export const PAUSE_PLAYER = 'pause_player';
export const STOP_PLAYER = 'stop_player';

export const SET_SEARCH_QUERY = 'set_search_query';
