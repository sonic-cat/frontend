import axios from 'axios';
import { history } from '../index';

import { ROOT_URL } from './config';
import { AUTH_USER, UNAUTH_USER, AUTH_ERROR, REGISTER_USER, REGISTER_ERROR } from './types';

export function loginUser({ email, password }) {
  return function(dispatch) {
    axios.post(`${ROOT_URL}/login`, { email, password })
      .then(response => {
        dispatch({ type: AUTH_USER, payload: response.data });
        localStorage.setItem('token', response.data.token);
        localStorage.setItem('user', JSON.stringify(response.data.user));
        // TODO : Redirect user to user dashboard
        history.push('/');
      })
      .catch(() => {
        dispatch(authError('Bad Login Info'));
      });
  };
}

export function logoutUser() {
  localStorage.removeItem('token');

  return { type: UNAUTH_USER };
}

export function authError(error) {
  // Redirect to login page.
  history.push('/login');

  return {
    type: AUTH_ERROR,
    payload: error
  };
}

export function regiserUser({ firstName, lastName, email, password }) {
  return function(dispatch) {
    axios.post(`${ROOT_URL}/register`, { first_name: firstName, last_name: lastName, email, password })
    .then(response => {
      dispatch({ type: REGISTER_USER, payload: response.data });
      localStorage.setItem('token', response.data.token);
      localStorage.setItem('user', JSON.stringify(response.data.user));
      // TODO : Redirect user to user dashboard
      history.push('/');
    })
    .catch((error) => {
      dispatch(registerError(error.response.data.errors));
    });
  };
}

export function registerError(error) {
  return {
    type: REGISTER_ERROR,
    payload: error
  };
}
