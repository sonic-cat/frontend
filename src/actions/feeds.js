import axios from 'axios';

import { ROOT_URL } from './config';

export function getFeeds() {
  const token = localStorage.getItem('token');

  return axios.get(`${ROOT_URL}/feeds`, {
    headers: {'Authorization': `User ${token}`},
  });
}

export function getFeed(id) {
  const token = localStorage.getItem('token');

  return axios.get(`${ROOT_URL}/feeds/${id}`, {
    headers: {'Authorization': `User ${token}`},
  });
}

export function addFeed(url) {
  const token = localStorage.getItem('token');

  return axios.post(`${ROOT_URL}/feeds`, { url }, {
    headers: {'Authorization': `User ${token}`},
  });
}
