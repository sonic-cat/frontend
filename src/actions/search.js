import { SET_SEARCH_QUERY } from '../actions/types';

export function setQuery(query) {
  return {
    type: SET_SEARCH_QUERY,
    payload: query
  };
}
