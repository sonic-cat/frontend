import { SET_PLAYLIST, PLAY_ENTRY, PAUSE_PLAYER, STOP_PLAYER } from './types';

export function setPlaylist(playlist) {
  return {
    type: SET_PLAYLIST,
    payload: playlist
  };
}

export function playEntry(playlistIndex) {
  return {
    type: PLAY_ENTRY,
    payload: playlistIndex
  };
}

export function pause() {
  return {
    type: PAUSE_PLAYER,
    payload: null
  };
}

export function stop() {
  return {
    type: STOP_PLAYER,
    payload: null
  };
}
