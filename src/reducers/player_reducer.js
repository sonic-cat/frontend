import { SET_PLAYLIST, PLAY_ENTRY, PAUSE_PLAYER, STOP_PLAYER } from '../actions/types';

export default function(state = {status: 'stopped', currentPlaylistEntry: null}, action) {
  switch(action.type) {
    case SET_PLAYLIST:
      return { ...state, playlist: action.payload };
    case PLAY_ENTRY:
      return { ...state, status: 'playing', currentPlaylistEntry: action.payload };
    case PAUSE_PLAYER:
      return { ...state, status: 'paused' };
    case STOP_PLAYER:
      return { ...state, status: 'stopped', currentPlaylistEntry: null };
    default:
      return state;
  }
}
