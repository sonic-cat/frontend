import { AUTH_USER, UNAUTH_USER, AUTH_ERROR, REGISTER_USER, REGISTER_ERROR } from '../actions/types';

export default function(state = {}, action) {
  switch(action.type) {
    case AUTH_USER:
      return { ...state, error: '', authenticated: true, user: action.payload.user };
    case UNAUTH_USER:
      return { ...state, error: '', authenticated: false, user: null };
    case AUTH_ERROR:
      return { ...state, error: action.payload };
    case REGISTER_USER:
      return { ...state, error: '', authenticated: true, user: action.payload.user };
    case REGISTER_ERROR:
      return { ...state, errors: action.payload };
    default:
      return state;
  }
}
