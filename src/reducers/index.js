import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';

import authReducer from './auth_reducer';
import playerReducer from './player_reducer';
import searchReducer from './search_reducer';

const rootReducer = combineReducers({
  form,
  auth: authReducer,
  player: playerReducer,
  search: searchReducer
});

export default rootReducer;
