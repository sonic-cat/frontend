import { SET_SEARCH_QUERY } from '../actions/types';

export default function(state = {query: null}, action) {
  switch(action.type) {
    case SET_SEARCH_QUERY:
      return { ...state, query: action.payload };
    default:
      return state;
  }
}
