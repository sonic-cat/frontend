import React, { Component } from 'react';
import { connect } from 'react-redux';
import fetchJsonp from 'fetch-jsonp';
import axios from 'axios';

import { ROOT_URL } from '../actions/config';
import '../styles/search.css';

class Search extends Component {
  constructor(props) {
    super(props);

    this.state = { results: [], isPosting: false, postCollectionId: null };
  }

  componentWillMount() {
    if (this.props.query && this.props.query !== '') {
      this.getSearchResults(this.props.query);
    }
  }

  componentWillReceiveProps(nextProps) {
    const query = nextProps.query;

    if (this.props.query !== nextProps.query) {
      if (this.searchDebounce) {
        window.clearTimeout(this.searchDebounce);
      }

      this.searchDebounce = window.setTimeout(() => {
        this.getSearchResults(query);
      }, 300);
    }
  }

  getSearchResults(query) {
    fetchJsonp(`https://itunes.apple.com/search?media=podcast&term=${encodeURIComponent(query)}`).then((response) => {
      return response.json();
    }).then((data) => {
      this.setState({ results: data.results });
    });
  }

  addFeed(result) {
    if (!this.state.postCollectionId) {
      const token = localStorage.getItem('token');
      this.setState({ isPosting: true, postCollectionId: result.collectionId });
      axios.post(`${ROOT_URL}/feeds`, { url: result.feedUrl }, {
        headers: {'Authorization': `User ${token}`},
      }).then(() => {
        this.props.history.push('/');
      }).catch(() => {
        this.setState({ errorMessage: 'Invalid URL.', isPosting: false, postCollectionId: null });
      });
    }
  }

  render() {
    const tableBody = this.state.results.map((result, index) => {
      return (
        <tr key={index}>
          <td>
            <img src={result.artworkUrl60} alt={result.collectionName}/>
          </td>
          <td>{result.collectionName}</td>
          <td>{result.artistName}</td>
          <td className="text-center">
            <button className="btn btn-primary" type="button" onClick={() => { this.addFeed(result); }}>
              { this.state.postCollectionId && this.state.postCollectionId === result.collectionId ? <i className="fa fa-spinner fa-spin"></i> : "Add" }
            </button>
          </td>
        </tr>
      );
    });

    const tableBodyMobile = this.state.results.map((result, index) => {
      return (
        <tr key={index} onClick={() => { this.addFeed(result); }}>
          <td>
            <img src={result.artworkUrl60} alt={result.collectionName}/>
          </td>
          <td>{result.collectionName}</td>
          <td>{result.artistName.length > 70 ? result.artistName.substring(0, 70) + '...' : result.artistName}</td>
        </tr>
      );
    });

    return (
      <div>
        <div className="container search hidden-sm-down">
          <h1><strong>Search results</strong>: {this.props.query}</h1>
          <table className="table">
            <thead>
              <tr>
                <th></th>
                <th>Title</th>
                <th>Author</th>
                <th className="hidden-sm-down"></th>
              </tr>
            </thead>
            <tbody>
              {tableBody}
            </tbody>
          </table>
        </div>
        <div className="container search hidden-md-up">
          <h1><strong>Search results</strong>: {this.props.query}</h1>
          <table className="table">
            <thead>
              <tr>
                <th></th>
                <th>Title</th>
                <th>Author</th>
                <th className="hidden-sm-down"></th>
              </tr>
            </thead>
            <tbody>
              {tableBodyMobile}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    query: state.search.query
  };
}

export default connect(mapStateToProps)(Search);
