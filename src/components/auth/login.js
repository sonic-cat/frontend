import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import * as actions from '../../actions/auth';
import logoImage from '../../images/sonicatlogo-1.svg';
import backgroundImage from '../../images/egor-khomiakov-242859.png';
import '../../styles/login.css';

class Login extends Component {

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'has-danger' : ''}`;

    return (
      <fieldset className={className}>
        <label>{field.label}</label>
        <input type={field.type} className="form-control" {...field.input} />
        {touched ? <small className="text-help text-muted">{error}</small> : ''}
      </fieldset>
    );
  }

  handleFormSubmit({ email, password }) {
    this.props.loginUser({ email, password });
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.errorMessage}
        </div>
      );
    }
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="d-flex login-card-container">
        <div className="d-flex login-card">
          <div className="login-card-image hidden-sm-down" style={{ backgroundImage: `url(${backgroundImage})` }}></div>
          <div className="login-card-form">
            <div className="login-form-container">
              <Link to="/">
                <img src={logoImage} alt="Sonicat"/>
              </Link>
              <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                <Field name="email" label="Email" type="email" component={this.renderField} />
                <Field name="password" label="Password" type="password" component={this.renderField} />
                {this.renderAlert()}
                <div className="row buttons-row">
                  <div className="col-6">
                    <button action="submit" className="btn btn-block btn-primary mr-4">Sign in</button>
                  </div>
                  <div className="col-6">
                    <Link to="/register" className="btn btn-block btn-link">Register</Link>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  if (!values.email) {
    errors.email = "Enter an email."
  }

  if (!values.password) {
    errors.password = "Enter a password."
  }

  return errors;
}

function mapStateToProps(state) {
  return { errorMessage: state.auth.error };
}

export default reduxForm({
  form: 'login',
  validate
})(connect(mapStateToProps, actions)(Login));
