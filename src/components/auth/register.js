import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import * as actions from '../../actions/auth';
import logoImage from '../../images/sonicatlogo-1.svg';
import backgroundImage from '../../images/egor-khomiakov-242859.png';
import '../../styles/login.css';

class Register extends Component {

  renderField(field) {
    const { meta: { touched, error }, serverErrors } = field;
    let serverErrorsMessages = [];
    let className = `form-group ${touched && error ? 'has-danger' : ''}`;

    if (serverErrors && serverErrors[field.input.name]) {
      className = 'form-group has-danger';
      serverErrorsMessages = serverErrors[field.input.name].map((error, index) => {
        return (
          <small key={index} className="text-help text-muted">{error}</small>
        );
      });
    }

    return (
      <fieldset className={className}>
        <label>{field.label}</label>
        <input type={field.type} className="form-control" {...field.input} />
        {touched ? <small className="text-help text-muted">{error}</small> : ''}
        {serverErrorsMessages}
      </fieldset>
    );
  }

  handleFormSubmit({ firstName, lastName, email, password }) {
    this.props.regiserUser({ firstName, lastName, email, password });
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="d-flex login-card-container">
        <div className="d-flex login-card">
          <div className="login-card-image hidden-sm-down" style={{ backgroundImage: `url(${backgroundImage})` }}></div>
          <div className="login-card-form">
            <div className="login-form-container">
              <Link to="/">
                <img src={logoImage} alt="Sonicat"/>
              </Link>
              <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                <div className="row">
                  <div className="col-6">
                    <Field name="firstName" label="First name" type="text" component={this.renderField} serverErrors={this.props.errors} />
                  </div>
                  <div className="col-6">
                    <Field name="lastName" label="Last name" type="text" component={this.renderField} serverErrors={this.props.errors} />
                  </div>
                </div>
                <Field name="email" label="Email" type="email" component={this.renderField} serverErrors={this.props.errors} />
                <Field name="emailConfirmation" label="Email confirmation" type="email" component={this.renderField} serverErrors={this.props.errors} />
                <Field name="password" label="Password" type="password" component={this.renderField} serverErrors={this.props.errors} />
                <Field name="passwordConfirmation" label="Password confirmation" type="password" component={this.renderField} serverErrors={this.props.errors} />
                <div className="row buttons-row">
                  <div className="col-6">
                    <button action="submit" className="btn btn-block btn-primary">Sign up</button>
                  </div>
                </div>
                <div className="text-center text-muted mt-4">
                  Already have an account? <Link to="/">Log in</Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  if (!values.firstName) {
    errors.firstName = "This field is required.";
  }

  if (!values.lastName) {
    errors.lastName = "This field is required.";
  }

  if (!values.email) {
    errors.email = "This field is required.";
  }

  if (!values.emailConfirmation) {
    errors.emailConfirmation = "This field is required.";
  } else if (values.email !== values.emailConfirmation) {
    errors.emailConfirmation = "Email confirmation does not match the email given."
  }

  if (!values.password) {
    errors.password = "This field is required.";
  }

  if (!values.passwordConfirmation) {
    errors.passwordConfirmation = "This field is required.";
  } else if (values.password !== values.passwordConfirmation) {
    errors.passwordConfirmation = "Password confirmation does not match the password given."
  }

  return errors;
}

function mapStateToProps(state) {
  return { errors: state.auth.errors };
}

export default reduxForm({
  form: 'register',
  validate
})(connect(mapStateToProps, actions)(Register));
