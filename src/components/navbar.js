import React, { Component } from 'react';
import {
  Collapse, Navbar, NavbarToggler, Nav, NavItem, NavLink,
  Dropdown, DropdownMenu, DropdownItem, Input
} from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import * as authActions from '../actions/auth';
import * as searchActions from '../actions/search';
import LogoImage from './../images/sonicatlogo-1-white.svg';
import './../styles/navbar.css';

class NavbarComponent extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleDropdown = this.toggleDropdown.bind(this);
    this.state = {
      isOpen: false,
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  toggleDropdown() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  onSearchChange(e) {
    this.props.history.push('/search');
    this.props.searchActions.setQuery(e.target.value);
  }

  render() {
    return (
      <div>
        <Navbar light toggleable>
          <NavbarToggler left onClick={this.toggle}>
            <i className="fa fa-bars" aria-hidden="true"></i>
          </NavbarToggler>

          <Link to="/" className="navbar-brand">
            <img src={LogoImage} alt="SoniCat"/>
          </Link>
          <Collapse isOpen={this.state.isOpen} navbar>
            <div className="form-inline search-form">
              <Input type="text" name="text" placeholder="Search new podcasts" onChange={this.onSearchChange.bind(this)} onFocus={(e) => { if (e.target.value !== '') this.props.history.push('/search'); }} />
            </div>
            {
              this.props.user ?
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggleDropdown}>
                    <NavLink onClick={this.toggleDropdown}
                             data-toggle="dropdown"
                             aria-haspopup="true"
                             aria-expanded={this.state.dropdownOpen}
                             className="nav-user"
                    >
                      <span>{this.props.user.first_name} {this.props.user.last_name}</span>
                    </NavLink>
                    <DropdownMenu right>
                      <DropdownItem onClick={() => { this.props.history.push('/'); }}>My Feeds</DropdownItem>
                      <DropdownItem divider/>
                      <DropdownItem onClick={() => { this.props.history.push('/feeds/add'); }}>Add Feed</DropdownItem>
                      <DropdownItem divider/>
                      <DropdownItem header>Account</DropdownItem>
                      <DropdownItem disabled>Edit profile</DropdownItem>
                      <DropdownItem onClick={this.props.authActions.logoutUser}>Logout</DropdownItem>
                    </DropdownMenu>
                  </Dropdown>
                </NavItem>
              </Nav> : null
            }
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch),
    searchActions: bindActionCreators(searchActions, dispatch)
  };
}

function mapStateToProps(state) {
  return {
    user: state.auth.user
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NavbarComponent));
