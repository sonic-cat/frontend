import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';

import * as feedsActions from '../actions/feeds';
import * as playerActions from '../actions/player';
import '../styles/feed.css';
import playImage from './../images/play.svg';
import pauseImage from './../images/pause.svg';

class FeedComponent extends Component {
  constructor(props) {
    super(props);

    this.state = { feed: null };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.id !== this.props.match.params.id) {
      feedsActions.getFeed(nextProps.match.params.id).then((response) => {
        this.setState({ feed: response.data.data });
      });
    }
  }

  componentWillMount() {
    feedsActions.getFeed(this.props.match.params.id).then((response) => {
      this.setState({ feed: response.data.data });
    });
  }

  playAllEntries() {
    this.playEntry(0);
  }

  playEntry(entryIndex) {
    const entries = this.state.feed.entries.map((entry) => {
      entry.feedName = this.state.feed.name;
      entry.feedImage = this.state.feed.image;
      return entry;
    });
    this.props.playerActions.setPlaylist(entries);
    this.props.playerActions.playEntry(entryIndex);
  }

  pauseEntry() {
    this.props.playerActions.pause();
  }

  renderEntriesTable() {
    const rows = this.state.feed.entries.map((entry, index) => {
      return (
        <tr key={entry.id}>
          <td>{entry.title}</td>
          <td className="text-nowrap">{moment(entry.date).format('YYYY-MM-DD')}</td>
          <td>
            {
              this.props.currentPlaylistEntry !== null && this.props.playerStatus === 'playing' && this.props.playlist[this.props.currentPlaylistEntry].id === entry.id ?
                <img src={pauseImage} className="play-button" alt="Pause" onClick={() => { this.pauseEntry(); }}/> :
                <img src={playImage} className="play-button" alt="Play" onClick={() => { this.playEntry(index); }}/>
            }
          </td>
        </tr>
      );
    });

    return (
      <table className="table">
        <thead>
          <tr>
            <th>Title</th>
            <th>Date</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
    );
  }

  render() {
    if (!this.state.feed) {
      return (
        <div className="text-center p-4">
          <i className="fa fa-spinner fa-spin fa-4x"></i>
          <div className="mt-3">Loading</div>
        </div>
      );
    }

    return (
      <div className="container feed-details">
        <div className="feed-header d-flex justify-content-between">
          <div className="feed-header-image">
            <img src={this.state.feed.image} alt={this.state.feed.name} />
          </div>
          <div className="feed-header-text">
            <h1>{this.state.feed.name}</h1>
            <p>{this.state.feed.description}</p>
            <h2>{this.state.feed.author}</h2>
          </div>
          <div className="feed-header-buttons">
            <button className="btn btn-primary" onClick={this.playAllEntries.bind(this)}>Play</button>
          </div>
        </div>

        {this.renderEntriesTable()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentPlaylistEntry: state.player.currentPlaylistEntry,
    playlist: state.player.playlist,
    playerStatus: state.player.status
  };
}

function mapDispatchToProps(dispatch) {
  return {
    playerActions: bindActionCreators(playerActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FeedComponent);
