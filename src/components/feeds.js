import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import * as feedsActions from '../actions/feeds';
import '../styles/feeds.css';

class FeedsComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      feeds: null
    };
  }

  componentWillMount() {
    feedsActions.getFeeds().then((response) => {
      this.setState({ feeds: response.data.data });
    });;
  }

  renderFeeds() {
    return this.state.feeds.map((feed) => {
      return (
        <div key={feed.id} className="col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-2 feed">
          <Link to={`/feeds/${feed.id}`}>
            <img src={feed.image} alt={feed.name} />
            <div className="feed-name">{feed.name}</div>
            <div className="feed-author">{feed.author}</div>
          </Link>
        </div>
      );
    });
  }

  render() {
    if (!this.state.feeds) {
      return (
        <div className="text-center p-4">
          <i className="fa fa-spinner fa-spin fa-4x"></i>
          <div className="mt-3">Loading</div>
        </div>
      );
    }

    if (this.state.feeds.length === 0) {
      return (
        <div className="container text-center py-5">
          <div>
            <i className="fa fa-bookmark fa-3x"></i>
          </div>
          <div className="py-3">
            You do not have any podcasts feeds yet! Try searching for a new podcast feed in the search bar above.
          </div>
        </div>
      );
    }

    return (
      <div className="feeds">
        <div className="row">
          {this.renderFeeds()}
        </div>
      </div>
    );
  }
}

export default FeedsComponent;
