import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import NavbarComponent from './navbar';
import PlayerComponent from './player';
import SearchComponent from './search';
import FeedsComponent from './feeds';
import FeedComponent from './feed';
import AddFeedComponent from './add_feed';

class Index extends Component {
  render() {
    return (
      <div>
        <NavbarComponent />
        <Switch>
          <Route path="/feeds/add" component={AddFeedComponent}/>
          <Route path="/feeds/:id" component={FeedComponent}/>
          <Route path="/search" component={SearchComponent}/>
          <Route path="/" component={FeedsComponent}/>
        </Switch>
        <PlayerComponent />
      </div>
    );
  }
}

export default Index;
