import React, { Component } from 'react';
import { connect } from 'react-redux';
import Slider from 'react-rangeslider';
import { Howl } from 'howler';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import moment from 'moment';
import 'moment-duration-format';

import * as playerActions from '../actions/player';
import volumeImage from './../images/audio.svg';
import volumeMutedImage from './../images/no-sound.svg';
import playImage from './../images/play.svg';
import pauseImage from './../images/pause.svg';
import forwardImage from './../images/frente.svg';
import backImage from './../images/tras.svg';
import nextImage from './../images/next.svg';
import nextDisabledImage from './../images/next-disabled.svg';
import lastImage from './../images/last.svg';
import lastDisabledImage from './../images/last-disabled.svg';
import 'react-rangeslider/lib/index.css';
import './../styles/player.css';

class PlayerComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentTime: 0,
      volume: 1.0,
      currentTrackId: null,
      isMuted: false,
      isMobileOpen: false
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.status !== this.props.status || !this.isSamePlaylist(prevProps.playlist, this.props.playlist) || prevProps.currentPlaylistEntry !== this.props.currentPlaylistEntry) {
      switch (this.props.status) {
        case "playing":
          if (prevProps.status === 'paused' && this.isSamePlaylist(prevProps.playlist, this.props.playlist) && prevProps.currentPlaylistEntry === this.props.currentPlaylistEntry) {
            this.player.play(this.state.currentTrackId);
          } else {
            this.playEntry();
          }
          break;
        case 'stopped':
          // TODO : Stop current player
          this.player.stop().unload();
          break;
        case 'paused':
          this.player.pause();
          break;
        default:
          console.log('Defaulting...');
      }
    }
  }

  isSamePlaylist(playlist, comparePlaylist) {
    if (!playlist || !comparePlaylist) return false;
    if (playlist.length !== comparePlaylist.length) return false;
    for (let i = 0; i < playlist.length; i++) {
      if (playlist[i].id !== comparePlaylist[i].id) return false;
    }
    return true;
  }

  playEntry = () => {
    const entry = this.props.playlist[this.props.currentPlaylistEntry];

    if (this.player) {
      this.player.stop().unload();
    }

    this.player = new Howl({
      src: [entry.url],
      html5: true,
      volume: this.state.volume
    });

    this.player.once('load', () => {
      console.log('duration', this.player.duration());
      this.setState({ duration: this.player.duration() });
    });

    this.player.on('play', (id) => {
      console.log('play');
      this.timer = setInterval(() => { this.setState({ currentTime: this.state.currentTime + 1 }); }, 1000);
      this.setState({ currentTrackId: id });
    });

    this.player.on('pause', () => {
      console.log('pause');
      window.clearTimeout(this.timer);
    });

    this.player.on('stop', () => {
      console.log('stop');
      window.clearTimeout(this.timer);
      if (this.state.duration > 0 && Math.abs(this.state.currentTime - this.state.duration) < 1 && this.props.currentPlaylistEntry < this.props.playlist.length - 1) {
        this.props.playerActions.playEntry(this.props.currentPlaylistEntry + 1);
      }

      if (this.state.duration > 0 && Math.abs(this.state.currentTime - this.state.duration) < 1 && this.props.currentPlaylistEntry === this.props.playlist.length - 1) {
        this.props.playerActions.stop();
      }

      this.setState({ duration: 0, currentTime: 0, currentTrackId: null });
    });

    this.player.play();
  }

  pause = () => {
    this.props.playerActions.pause();
  }

  resume = () => {
    this.props.playerActions.playEntry(this.props.currentPlaylistEntry);
  }

  previousEntry = () => {
    if (this.props.currentPlaylistEntry > 0) {
      this.props.playerActions.playEntry(this.props.currentPlaylistEntry - 1);
    }
  }

  nextEntry = () => {
    if (this.props.currentPlaylistEntry < this.props.playlist.length) {
      this.props.playerActions.playEntry(this.props.currentPlaylistEntry + 1);
    }
  }

  goBack15 = () => {
    if (this.state.currentTime >= 15) {
      this.setState({ currentTime: this.state.currentTime - 15 });
      this.player.seek(this.state.currentTime - 15);
    } else {
      this.setState({ currentTime: 0 });
      this.player.seek(0);
    }
  }

  goForward15 = () => {
    if (this.state.currentTime <= this.state.duration - 15) {
      this.setState({ currentTime: this.state.currentTime + 15 });
      this.player.seek(this.state.currentTime + 15);
    } else {
      this.setState({ currentTime: this.state.duration });
      this.player.seek(this.state.duration);
    }
  }

  onSeek = (value) => {
    this.setState({ currentTime: value });
    this.player.seek(value);
  }

  onVolumeChange = (value) => {
    this.setState({ volume: value });
    this.player.volume(value);
  }

  onMuteClick = () => {
    this.player.mute(!this.state.isMuted, this.state.currentTrackId);
    this.setState({ isMuted: !this.state.isMuted });
  }

  onOpenMobilePlayer = () => {
    this.setState({ isMobileOpen: true });
  }

  onCloseMobilePlayer = () => {
    this.setState({ isMobileOpen: false });
  }

  render() {
    if (this.props.currentPlaylistEntry === null) {
      return null;
    }

    const entry = this.props.playlist[this.props.currentPlaylistEntry];
    const mobileClasses = this.state.isMobileOpen ? '' : 'hidden-sm-down';
    const playerClasses = `d-flex player align-items-center ${mobileClasses}`;

    return (
      <div>
        <div className="d-flex mini-player hidden-md-up">
          <div onClick={this.onOpenMobilePlayer}>
            <img className="player-cover" src={entry.feedImage} alt={entry.feedName} />
          </div>
          <div className="d-flex mini-player-info" onClick={this.onOpenMobilePlayer}>
            <div className="mini-player-info-title">
              {entry.title.length > 70 ? entry.title.substring(0, 70) + '...' : entry.title}
            </div>
            <div className="mini-player-info-time">
              <span>
                {moment.duration(this.state.currentTime, "seconds").format("hh:mm:ss", { trim: false })}
              </span>
            </div>
          </div>
          <div className="mini-player-button">
            {
              this.props.status === 'playing' ?
                <img src={pauseImage} alt="Pause" onClick={this.pause}/> :
                <img src={playImage} alt="Play" onClick={this.resume}/>
            }
          </div>
        </div>
        <div className={playerClasses}>
          <div className="d-flex align-items-center player-info-container">
            <img className="player-cover" src={entry.feedImage} alt={entry.feedName} />
            <div className="player-info">
              <div className="player-info-entry">
                {entry.title.length > 70 ? entry.title.substring(0, 70) + '...' : entry.title}
              </div>
              <div className="player-info-feed">
                <Link to={`/feeds/${entry.feed_id}`}>
                  {entry.feedName.length > 60 ? entry.feedName.substring(0, 60) + '...' : entry.feedName}
                </Link>
              </div>
            </div>
          </div>

          <div className="d-flex flex-column player-controls">
            <div className="d-flex justify-content-center player-controls-buttons">
              {
                this.props.currentPlaylistEntry === 0 ?
                  <img src={lastDisabledImage} alt="Previous"/> :
                  <img src={lastImage} alt="Previous" onClick={this.previousEntry}/>
              }
              <img src={backImage} alt="Back 15 seconds" onClick={this.goBack15}/>
              {
                this.props.status === 'playing' ?
                  <img src={pauseImage} alt="Pause" onClick={this.pause}/> :
                  <img src={playImage} alt="Play" onClick={this.resume}/>
              }
              <img src={forwardImage} alt="Forward 15 seconds" onClick={this.goForward15}/>
              {
                this.props.currentPlaylistEntry === this.props.playlist.length - 1 ?
                  <img src={nextDisabledImage} alt="Next"/> :
                  <img src={nextImage} alt="Next" onClick={this.nextEntry}/>
              }
            </div>
            <div>
              <Slider
                value={this.state.currentTime}
                onChange={this.onSeek}
                min={0}
                max={this.state.duration}
                tooltip={false}
              />
              <span>
                {moment.duration(this.state.currentTime, "seconds").format("hh:mm:ss", { trim: false })}
              </span>
              <span className="float-right">
                {moment.duration(this.state.duration, "seconds").format("hh:mm:ss", { trim: false })}
              </span>
            </div>
          </div>

          <div className="player-volume d-flex">
            {
              this.state.isMuted ?
                <img src={volumeMutedImage} onClick={this.onMuteClick} alt="Volume"/> :
                <img src={volumeImage} onClick={this.onMuteClick} alt="Volume"/>
            }
            <Slider
              value={this.state.volume}
              onChange={this.onVolumeChange}
              min={0}
              max={1}
              step={0.01}
              tooltip={false}
            />
          </div>
          <div className="player-close hidden-md-up">
            <i className="fa fa-times" onClick={this.onCloseMobilePlayer}></i>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    playlist: state.player.playlist,
    status: state.player.status,
    currentPlaylistEntry: state.player.currentPlaylistEntry
  };
}

function mapDispatchToProps(dispatch) {
  return {
    playerActions: bindActionCreators(playerActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerComponent);
