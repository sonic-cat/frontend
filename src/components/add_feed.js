import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { withRouter } from 'react-router-dom';
import axios from 'axios';

import { ROOT_URL } from '../actions/config';

class AddFeed extends Component {
  constructor(props) {
    super(props);
    this.state = { errorMessage: '', isPosting: false };
  }

  renderField(field) {
    const { meta: { touched, error } } = field;
    let serverErrors = [];
    let className = `form-group ${touched && error ? 'has-danger' : ''}`;

    if (this.props.errors && this.props.errors[field.input.name]) {
      className = 'form-group has-danger';
      serverErrors = this.props.errors[field.input.name].map((error, index) => {
        return (
          <small key={index} className="text-help text-muted">{error}</small>
        );
      });
    }

    return (
      <fieldset className={className}>
        <label>{field.label}</label>
        <input type={field.type} className="form-control" {...field.input} />
        {touched ? <small className="text-help text-muted">{error}</small> : ''}
        {serverErrors}
      </fieldset>
    );
  }

  handleFormSubmit({ url }) {
    const token = localStorage.getItem('token');

    this.setState({ isPosting: true });
    axios.post(`${ROOT_URL}/feeds`, { url }, {
      headers: {'Authorization': `User ${token}`},
    }).then(() => {
      this.props.history.push('/');
    }).catch(() => {
      this.setState({ errorMessage: 'Invalid URL.', isPosting: false });
    });
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="container">
        <h1 className="text-center">Add Feed</h1>
        { this.state.errorMessage ? <div className="alert alert-danger">{this.state.errorMessage}</div> : null }
        <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
          <Field name="url" label="Feed URL" type="text" component={this.renderField.bind(this)} />
          <button action="submit" className="btn btn-primary">Add</button>
          { this.state.isPosting ? <span className="ml-4">We've dispatched a sonic cat to retrieve your feed, please wait. <i className="fa fa-spinner fa-spin ml-2"></i></span> : null }
        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  if (!values.url) {
    errors.url = "This field is required.";
  }

  return errors;
}

export default reduxForm({
  form: 'add-feed',
  validate
})(withRouter(AddFeed));
